package es.pildoras.conexionHibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ConsultaClientes {

	public static void main(String[] args) {
		
		SessionFactory miFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Clientes.class).buildSessionFactory();

		Session miSession = miFactory.openSession();
		
		try {
			
			//Comenzar sesi�n
			
			miSession.beginTransaction();
			
			//Consulta de Clientes
			
			List<Clientes>losClientes = miSession.createQuery("from Clientes").getResultList();
			
			//Mostrar los Clientes
			
			recorreClientesConsultados(losClientes);
			
			//Consulta: traer los Perez
			
			losClientes = miSession.createQuery("from Clientes cl where cl.apellidos= 'Perez' ").getResultList();
			
			//mostrar los Perez
			
			recorreClientesConsultados(losClientes);
			
			//muestra los que viven en Calle 123 o de apellido Sanchez
			
			losClientes = miSession.createQuery("from Clientes cl where cl.apellidos= 'Sanchez' "+
			"or cl.direccion = 'Calle 123'").getResultList();
			
			recorreClientesConsultados(losClientes);
			
			miSession.getTransaction().commit();
			
			//cerrar la Sesi�n
			
			miSession.close();
			
		}finally {
			
			miSession.close();
			
		}
	}

	private static void recorreClientesConsultados(List<Clientes> losClientes) {
		for (Clientes unCliente: losClientes) {
			
			System.out.println(unCliente);
		}
	}

}
