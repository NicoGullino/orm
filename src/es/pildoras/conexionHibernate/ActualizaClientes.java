package es.pildoras.conexionHibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ActualizaClientes {

	public static void main(String[] args) {
		
		SessionFactory miFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Clientes.class).buildSessionFactory();

		Session miSession = miFactory.openSession();
		
		try {
			
			//int ClienteId = 1;
						
			miSession.beginTransaction();
						
			//Clientes miCliente = miSession.get(Clientes.class, ClienteId);
			
			//miCliente.setNombre("Roberto");
			
			miSession.createQuery("update Clientes set Apellidos='Alcaraz' where Apellidos like 'L%'").executeUpdate();
			
			miSession.createQuery("delete Clientes where Direccion='Calle 778'").executeUpdate();
			
			miSession.getTransaction().commit();
			
			System.out.println("Registros actualizado correctamente en Base de Datos");
			
			miSession.close();
			
		}finally {
			
			miFactory.close();
			System.out.println();
			
		}
	}

}
